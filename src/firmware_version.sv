//                              -*- Mode: Verilog -*-
// Filename        : Firmware_version.sv
// Description     : Control block of the Caribou.
// Author          : Adrian Fiergolski
// Created On      : Tue May  9 16:02:31 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module firmware_version  #(AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 3)
   (
    ///////////////////////
    //AXI4-Lite interface
    ///////////////////////

    //Write address channel
	input logic [AXI_ADDR_WIDTH-1 : 0]     awaddr,
	input logic [2 : 0] 		       awprot,
	input logic 			       awvalid,
	output logic 			       awready,

    //Write data channel
	input logic [AXI_DATA_WIDTH-1 : 0]     wdata,
	input logic [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
	input logic 			       wvalid,
	output logic 			       wready,
   
    //Write response channel
	output logic [1 : 0] 		       bresp,
	output logic 			       bvalid,
	input logic 			       bready,

    //Read address channel
	input logic [AXI_ADDR_WIDTH-1 : 0]     araddr,
	input logic [2 : 0] 		       arprot,
	input logic 			       arvalid,
	output logic 			       arready,

    //Read data channel
	output logic [AXI_DATA_WIDTH-1 : 0]    rdata,
	output logic [1 : 0] 		       rresp,
	output logic 			       rvalid,
	input logic 			       rready,
   
	input logic 			       aclk,
	input logic 			       aresetN

    );
   
   ///////////////////////
   // AXI Lite interface
   ///////////////////////

   AXI4LiteInterface  #(AXI_DATA_WIDTH, AXI_ADDR_WIDTH) axi(aclk, aresetN);
   //Write address channel
   assign axi.awaddr = awaddr;
   assign axi.awprot = awprot;
   assign axi.awvalid = awvalid;
   assign awready = axi.awready;

   //Write data channel
   assign axi.wdata = wdata;
   assign axi.wstrb = wstrb;
   assign axi.wvalid = wvalid;
   assign wready = axi.wready;
   
   //Write response channel
   assign bresp = axi.bresp;
   assign bvalid = axi.bvalid;
   assign axi.bready = bready;

   //Read address channel
   assign axi.araddr = araddr;
   assign axi.arprot = arprot;
   assign axi.arvalid = arvalid;
   assign arready = axi.arready;

   //Read data channel
   assign rdata = axi.rdata;
   assign rresp = axi.rresp;
   assign rvalid = axi.rvalid;
   assign axi.rready = rready;

   firmware_version_AXI axi_fsm ( .* );

endmodule // Firmware_version

